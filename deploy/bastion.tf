data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = var.ami_owners

  dynamic "filter" {
    for_each = var.ami_filters
    iterator = tag
    content {
      name   = "${tag.key}"
      values = "${tag.value}"
    }
  }
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.bastion_instance_type
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}