variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "shah.soham@outlook.com"
}

variable "bastion_instance_type" {
  default = "t2.micro"
}

variable "ami_filters" {
  default = {
    "name" = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
  }
}

variable "ami_owners" {
  default = ["amazon"]
}